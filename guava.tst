##gap> START_TEST("arbitrary identifier string");

###################### GUAVA test file
##
## Created 02-2006; last modified 18-03-2008
##

Print("\n AClosestVectorCombinationsMatFFEVecFFE\n");
F:=GF(3);; x:= Indeterminate( F );; pol:= x^2+1;
C := GeneratorPolCode(pol,8,F);
v:=Codeword("12101111");
v:=VectorCodeword(v);
G:=GeneratorMat(C);
AClosestVectorCombinationsMatFFEVecFFE(G,F,v,1,1);

Print("\n AClosestVectorCombinationsMatFFEVecFFECoords\n");
F:=GF(3);; x:= Indeterminate( F );; pol:= x^2+1;
C := GeneratorPolCode(pol,8,F);
v:=Codeword("12101111"); v:=VectorCodeword(v);;
G:=GeneratorMat(C);;
AClosestVectorCombinationsMatFFEVecFFECoords(G,F,v,1,1);

Print("\n DistancesDistributionMatFFEVecFFE\n");
v:=[ Z(3)^0, Z(3), Z(3)^0, 0*Z(3), Z(3)^0, Z(3)^0, Z(3)^0, Z(3)^0 ];;
vecs:=[ [ Z(3)^0, 0*Z(3), Z(3)^0, 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3) ],
   [ 0*Z(3), Z(3)^0, 0*Z(3), Z(3)^0, 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3) ],
   [ 0*Z(3), 0*Z(3), Z(3)^0, 0*Z(3), Z(3)^0, 0*Z(3), 0*Z(3), 0*Z(3) ],
   [ 0*Z(3), 0*Z(3), 0*Z(3), Z(3)^0, 0*Z(3), Z(3)^0, 0*Z(3), 0*Z(3) ],
   [ 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3), Z(3)^0, 0*Z(3), Z(3)^0, 0*Z(3) ],
   [ 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3), Z(3)^0, 0*Z(3), Z(3)^0 ] ];;
DistancesDistributionMatFFEVecFFE(vecs,GF(3),v);

Print("\n DistancesDistributionVecFFEsVecFFE\n");
v:=[ Z(3)^0, Z(3), Z(3)^0, 0*Z(3), Z(3)^0, Z(3)^0, Z(3)^0, Z(3)^0 ];;
vecs:=[ [ Z(3)^0, 0*Z(3), Z(3)^0, 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3) ],
   [ 0*Z(3), Z(3)^0, 0*Z(3), Z(3)^0, 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3) ],
   [ 0*Z(3), 0*Z(3), Z(3)^0, 0*Z(3), Z(3)^0, 0*Z(3), 0*Z(3), 0*Z(3) ],
   [ 0*Z(3), 0*Z(3), 0*Z(3), Z(3)^0, 0*Z(3), Z(3)^0, 0*Z(3), 0*Z(3) ],
   [ 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3), Z(3)^0, 0*Z(3), Z(3)^0, 0*Z(3) ],
   [ 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3), 0*Z(3), Z(3)^0, 0*Z(3), Z(3)^0 ] ];;
DistancesDistributionVecFFEsVecFFE(vecs,v);

Print("\n DistanceVecFFE\n");
v1:=[ Z(3)^0, Z(3), Z(3)^0, 0*Z(3), Z(3)^0, Z(3)^0, Z(3)^0, Z(3)^0 ];;
v2:=[ Z(3), Z(3)^0, Z(3)^0, 0*Z(3), Z(3)^0, Z(3)^0, Z(3)^0, Z(3)^0 ];;
DistanceVecFFE(v1,v2);

Print("\n Codes\n");
C:=RandomLinearCode(20,10,GF(4));
c:=Random(C);
NamesOfComponents(C);
NamesOfComponents(c);
c!.VectorCodeword;
Display(last);
C!.Dimension;

Print("\n Codeword\n");
c := Codeword([0,1,1,1,0]);
VectorCodeword( c ); 
c2 := Codeword([0,1,1,1,0], GF(3));
VectorCodeword( c2 );
Codeword([c, c2, "0110"]);
p := UnivariatePolynomial(GF(2), [Z(2)^0, 0*Z(2), Z(2)^0]);
Codeword(p);

Print("\n Codeword2\n");
C := WholeSpaceCode(7,GF(5));
Codeword(["0220110", [1,1,1]], C);
Codeword(["0220110", [1,1,1]], 7, GF(5));
C:=RandomLinearCode(10,5,GF(3));
Codeword("1000000000",C);
Codeword("1000000000",10,GF(3));

Print("\n CodewordNr\n");
B := BinaryGolayCode();
c := CodewordNr(B, 4);
R := ReedSolomonCode(2,2);
AsSSortedList(R);
CodewordNr(R, [1,3]);

Print("\n IsCodeword\n");
IsCodeword(1);
IsCodeword(ReedMullerCode(2,3));
IsCodeword("11111");
IsCodeword(Codeword("11111"));

Print("\n Codewords EQ\n");
P := UnivariatePolynomial(GF(2), Z(2)*[1,0,0,1]);
c := Codeword(P, GF(2));
P = c;        # codeword operation
c2 := Codeword("1001", GF(2));
c = c2;
C:=HammingCode(3);
c1:=Random(C);
c2:=Random(C);
EQ(c1,c2);
not EQ(c1,c2);

Print("\n Codewords +\n");
C:=RandomLinearCode(10,5,GF(3));
c:=Random(C);
Codeword(c+"2000000000");

Print("\n Codewords +, 2\n");
C:=RandomLinearCode(10,5);
c:=Random(C);
c+C;
c+C=C;
IsLinearCode(c+C);
v:=Codeword("100000000");
v+C;
C=v+C;
C := GeneratorMatCode( [ [1, 0,0,0], [0, 1,0,0] ], GF(2) );
Elements(C);
v:=Codeword("0011");
C+v;
Elements(C+v);

Print("\n PolyCodeword\n");
a := Codeword("011011");; 
PolyCodeword(a);

Print("\n TreatAsVector\n");
B := BinaryGolayCode();
c := CodewordNr(B, 4);
TreatAsVector(c);
c;

Print("\n TreatAsPoly\n");
a := Codeword("00001",GF(2));
TreatAsPoly(a); a;
b := NullWord(6,GF(4));
TreatAsPoly(b); b;

Print("\n NullWord\n");
NullWord(8);
Codeword("0000") = NullWord(4);
NullWord(5,GF(16));
NullWord(ExtendedTernaryGolayCode());


Print("\n DistanceCodeword\n");
a := Codeword([0, 1, 2, 0, 1, 2]);; b := NullWord(6, GF(3));;
DistanceCodeword(a, b);
DistanceCodeword(b, a);
DistanceCodeword(a, a);

Print("\n WeightCodeword\n");
WeightCodeword(Codeword("22222"));
WeightCodeword(NullWord(3));
C := HammingCode(3);
Minimum(List(AsSSortedList(C){[2..Size(C)]}, WeightCodeword ) );

Print("\n ElementsCodes\n");
C := ElementsCode(["1100", "1010", "0001"], "example code", GF(2) );
MinimumDistance(C);
C;

Print("\n IsLinearCode\n");
L := Z(2)*[ [0,0,0], [1,0,0], [0,1,1], [1,1,1] ];;
C := ElementsCode( L, GF(2) );
IsLinearCode( C );
C;

Print("\n Decode\n");
R := ReedMullerCode( 1, 3 );
w := [ 1, 0, 1, 1 ] * R;
Decode( R, w );
Decode( R, w + "10000000" ); 

Print("\n Codes =\n");
M := [ [0, 0], [1, 0], [0, 1], [1, 1] ];;
C1 := ElementsCode( M, GF(2) );
M = C1;
C2 := GeneratorMatCode( [ [1, 0], [0, 1] ], GF(2) );
C1 = C2;
ReedMullerCode( 1, 3 ) = HadamardCode( 8 );
WholeSpaceCode( 5, GF(4) ) = WholeSpaceCode( 5, GF(2) );

Print("\n EvaluationCode \n");
F:=GF(11);
R := PolynomialRing(F,2);
indets := IndeterminatesOfPolynomialRing(R);;
x:=indets[1];; y:=indets[2];;
L:=[x^2*y,x*y,x^5,x^4,x^3,x^2,x,x^0];;
Pts:=[ [ Z(11)^9, Z(11) ], [ Z(11)^8, Z(11) ], [ Z(11)^7, 0*Z(11) ],\
[ Z(11)^6, 0*Z(11) ], [ Z(11)^5, 0*Z(11) ], [ Z(11)^4, 0*Z(11) ],\
[ Z(11)^3, Z(11) ], [ Z(11)^2, 0*Z(11) ], [ Z(11), 0*Z(11) ],\
[ Z(11)^0, 0*Z(11) ], [ 0*Z(11), Z(11) ] ];;
C:=EvaluationCode(Pts,L,R);
##MinimumDistance(C);

Print("\n DivisorOnAffineCurve \n");
F:=GF(11);;
R := PolynomialRing(F,2);;
indets := IndeterminatesOfPolynomialRing(R);;
x:=indets[1];; y:=indets[2];;
crv:=AffineCurve(y^2-x^11+x,R);
Pts:=AffinePointsOnCurve(y^2-x^11+x,R,F);
#q:=5;
#F:=GF(q);
#R:=PolynomialRing(F,2);;
#vars:=IndeterminatesOfPolynomialRing(R);
#x:=vars[1];
#y:=vars[2];
#crv:=AffineCurve(y^3-x^3-x-1,R);
#Pts:=AffinePointsOnCurve(crv,R,F);;
supp:=[Pts[1],Pts[2]];
D:=DivisorOnAffineCurve([1,-1],supp,crv);

Print("\n Divisors On Affine Curves, 2 \n");
F:=GF(11);
R1:=PolynomialRing(F,1);;
var1:=IndeterminatesOfPolynomialRing(R1);; a:=var1[1];;
b:=X(F,var1);
var2:=Concatenation(var1,[b]);
R2:=PolynomialRing(F,var2);
crvP1:=AffineCurve(b,R2);
div1:=DivisorOnAffineCurve([1,2,3,4],[Z(11)^2,Z(11)^3,Z(11)^7,Z(11)],crvP1);
DivisorDegree(div1);
div2:=DivisorOnAffineCurve([1,2,3,4],[Z(11),Z(11)^2,Z(11)^3,Z(11)^4],crvP1);
DivisorDegree(div2);
div3:=DivisorAddition(div1,div2);
DivisorDegree(div3);
DivisorIsEffective(div1);
DivisorIsEffective(div2);
ndiv1:=DivisorNegate(div1);
zdiv:=DivisorAddition(div1,ndiv1);
DivisorIsZero(zdiv);
div_gcd:=DivisorGCD(div1,div2);
div_lcm:=DivisorLCM(div1,div2);
DivisorDegree(div_gcd);
DivisorDegree(div_lcm);
DivisorEqual(div3,DivisorAddition(div_gcd,div_lcm));

Print("\n DivisorOfRationalFunctionP1 \n");
F:=GF(11);
R1:=PolynomialRing(F,1);;
var1:=IndeterminatesOfPolynomialRing(R1);; a:=var1[1];;
b:=X(F,var1);
var2:=Concatenation(var1,[b]);
R2:=PolynomialRing(F,var2);
pt:=Z(11);
f:=RiemannRochSpaceBasisFunctionP1(pt,2,R2);
Df:=DivisorOfRationalFunctionP1(f,R2);
Df.support;
F:=GF(11);;
R:=PolynomialRing(F,2);;
vars:=IndeterminatesOfPolynomialRing(R);;
a:=vars[1];;
b:=vars[2];;
f:=(a^4+Z(11)^6*a^3-a^2+Z(11)^7*a+Z(11)^0)/(a^4+Z(11)^4*a^3+Z(11)*a^2+Z(11)^7*a+Z(11));;
divf:=DivisorOfRationalFunctionP1(f,R);
denf:=DenominatorOfRationalFunction(f); RootsOfUPol(denf);
numf:=NumeratorOfRationalFunction(f); RootsOfUPol(numf);

Print("\n ActionMoebiusTransformationOnFunction\n");
F:=GF(11);
R1:=PolynomialRing(F,1);;
var1:=IndeterminatesOfPolynomialRing(R1);; a:=var1[1];;
b:=X(F,var1);
var2:=Concatenation(var1,[b]);
R2:=PolynomialRing(F,var2);
crvP1:=AffineCurve(b,R2);
D:=DivisorOnAffineCurve([1,2,3,4],[Z(11)^2,Z(11)^3,Z(11)^7,Z(11)],crvP1);
A:=Z(11)^0*[[1,2],[1,4]];
ActionMoebiusTransformationOnDivisorDefinedP1(A,D);
A:=Z(11)^0*[[1,2],[3,4]];
ActionMoebiusTransformationOnDivisorDefinedP1(A,D);
ActionMoebiusTransformationOnDivisorP1(A,D);
f:=MoebiusTransformation(A,R1);
ActionMoebiusTransformationOnFunction(A,f,R1);

Print("\n GoppaCodeClassical\n");
F:=GF(11);;
R2:=PolynomialRing(F,2);;
vars:=IndeterminatesOfPolynomialRing(R2);; 
a:=vars[1];;b:=vars[2];;
cdiv:=[ 1, 2, -1, -2 ];
sdiv:=[ Z(11)^2, Z(11)^3, Z(11)^6, Z(11)^9 ];
crv:=rec(polynomial:=b,ring:=R2);
div:=DivisorOnAffineCurve(cdiv,sdiv,crv);
pts:=Difference(Elements(GF(11)),div.support);
C:=GoppaCodeClassical(div,pts);
MinimumDistance(C);

Print("\n OnePointAGCode\n");
F:=GF(11);
R := PolynomialRing(F,2);
indets := IndeterminatesOfPolynomialRing(R);
x:=indets[1]; y:=indets[2];
P:=AffinePointsOnCurve(y^2-x^11+x,R,F);;
C:=OnePointAGCode(y^2-x^11+x,P,15,R);
Cd := DualCode(C);
MinimumDistance(Cd);
Pts:=List([1,2,4,6,7,8,9,10,11],i->P[i]);;
C:=OnePointAGCode(y^2-x^11+x,Pts,10,R);
Cd := DualCode(C);
MinimumDistance(Cd);

Print("\n Punctured Expurgated Augmented code\n");
C1 := PuncturedCode( ReedMullerCode( 1, 4 ) );
C2 := BCHCode( 15, 7, GF(2) );
C2 = C1;
p := CodeIsomorphism( C1, C2 );
C3 := PermutedCode( C1, p );
C2 = C3;
C1 := HammingCode( 4 );; WeightDistribution( C1 );
L := Filtered( AsSSortedList(C1), i -> WeightCodeword(i) = 3 );;
C2 := ExpurgatedCode( C1, L );
WeightDistribution( C2 );
C31 := ReedMullerCode( 1, 3 );
C32 := AugmentedCode(C31,["00000011","00000101","00010001"]);
C32 = ReedMullerCode( 2, 3 );
C1 := CordaroWagnerCode(6);
Codeword( [0,0,1,1,1,1] ) in C1;
C2 := AugmentedCode( C1 );
Codeword( [1,1,0,0,0,0] ) in C2;

Print("\n direct sum code\n");
C1 := ElementsCode( [ [1,0], [4,5] ], GF(7) );;
C2 := ElementsCode( [ [0,0,0], [3,3,3] ], GF(7) );;
D := DirectSumCode(C1, C2);;
AsSSortedList(D);
D = C1 + C2;   # addition = direct sum

Print("\n lower bound min dist\n");
C := BCHCode( 45, 7 );
LowerBoundMinimumDistance( C );
LowerBoundMinimumDistance( 45, 23, GF(2) );


Print("\n MatrixRepresentationOnRiemannRochSpaceP1\n");
F:=GF(11);
R1:=PolynomialRing(F,1);;
var1:=IndeterminatesOfPolynomialRing(R1);; a:=var1[1];;
b:=X(F,var1);
var2:=Concatenation(var1,[b]);
R2:=PolynomialRing(F,var2);
crvP1:=AffineCurve(b,R2);
D:=DivisorOnAffineCurve([1,1,1,4],[Z(11)^2,Z(11)^3,Z(11)^7,Z(11)],crvP1);
div:=D;
agp:=DivisorAutomorphismGroupP1(D);; ## slow
##IdGroup(agp);            ## requires small groups database
g:=Random(agp);
rho:=MatrixRepresentationOnRiemannRochSpaceP1(g,D);
Display(rho);
Eigenvalues(F,rho);
charpoly:=CharacteristicPolynomial(rho);
Factors(charpoly);
JordanDecomposition(rho);


Print("\n UUVCode\n");
C1 := EvenWeightSubcode(WholeSpaceCode(4, GF(2)));
C2 := RepetitionCode(4, GF(2));
R := UUVCode(C1, C2);
R = ReedMullerCode(1,3);

Print("\n LexiCode\n");
C := LexiCode( 4, 3, GF(5) );
IsLinearCode(C);
B := [ [Z(2)^0, 0*Z(2), 0*Z(2)], [Z(2)^0, Z(2)^0, 0*Z(2)] ];;
C := LexiCode( B, 2, GF(2) );
IsLinearCode(C);

Print("\n GeneratorMatCode\n");
G := Z(3)^0 * [[1,0,1,2,0],[0,1,2,1,1],[0,0,1,2,1]];;
C1 := GeneratorMatCode( G, GF(3) );
C2 := GeneratorMatCode( IdentityMat( 5, GF(2) ), GF(2) );
C3 := GeneratorMatCode(List(AsSSortedList(NordstromRobinsonCode()),x ->VectorCodeword(x)),GF(2));

Print("\n CheckMatCode\n");
H := Z(3)^0 * [[1,0,1,2,0],[0,1,2,1,1],[0,0,1,2,1]];;
C1 := CheckMatCode( H, GF(3) );
CheckMat(C1);
C2 := CheckMatCode( IdentityMat( 5, GF(2) ), GF(2) );

Print("\n AlternantCode\n");
Y := [ 1, 1, 1, 1, 1, 1, 1];; 
a := PrimitiveUnityRoot( 2, 7 );;
alpha := List( [0..6], i -> a^i );;
C := AlternantCode( 2, Y, alpha, GF(8) );

Print("\n RandomLinearCode\n");
C := RandomLinearCode( 15, 4, GF(3) ); 
Display(C); 
C := RandomLinearCode( 35, 20, GF(3) ); 
Display(C); 

Print("\n EvaluationBivariateCode\n");
q:=4;;
F:=GF(q^2);;
R:=PolynomialRing(F,2);;
vars:=IndeterminatesOfPolynomialRing(R);;
x:=vars[1];;
y:=vars[2];;
crv:=AffineCurve(y^q+y-x^(q+1),R);
L:=[ x^0, x, x^2*y^-1 ];
Pts:=AffinePointsOnCurve(crv.polynomial,crv.ring,F);;
C1:=EvaluationBivariateCode(Pts,L,crv); 
P:=Difference(Pts,[[ 0*Z(2^4)^0, 0*Z(2)^0 ]]);;
C2:=EvaluationBivariateCodeNC(P,L,crv); 
C3:=EvaluationCode(P,L,R);
MinimumDistance(C1);
MinimumDistance(C2);
MinimumDistance(C3);

## gap> STOP_TEST( "filename", 10000 );