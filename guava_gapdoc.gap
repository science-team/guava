############################################################
#
# commands to create GUAVA documentation using GAPDoc 0.99
#
###########################################################


path := Directory("doc"); ## edit this path if needed
main:="guava.xml"; 
files:=[];
bookname:="guava";
#str := ComposedXMLString(path, main, files);;
#r := ParseTreeXMLString(str);; 
######### with break here if there is an xml compiling error #########
#CheckAndCleanGapDocTree(r);
#l := GAPDoc2LaTeX(r);;
#FileString(Filename(path, Concatenation(bookname, ".tex")), l);
MakeGAPDocDoc( path, main, files, bookname);
