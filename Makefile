CC      = gcc
CFLAGS  = -O2
SRCDIR  = ../../src/leon/src
CJSRCDIR= ../../src/ctjhai

GAP_PATH=../..
PKG_PATH=..
SRCDISTFILE=guava

all:
	( test -d bin || mkdir bin; \
	test -d bin/x86_64-unknown-linux-gnu-gcc || mkdir bin/x86_64-unknown-linux-gnu-gcc; cd bin/x86_64-unknown-linux-gnu-gcc; \
	$(MAKE) -f ../../Makefile all2 CC="$(CC)" CFLAGS="$(CFLAGS)"; \
	cd $(SRCDIR)/../; ./configure; $(MAKE); mkdir ../../bin/leon; \
	cp cent ../../bin/leon; cp cjrndper ../../bin/leon; \
	cp commut ../../bin/leon; cp compgrp ../../bin/leon; \
	cp desauto ../../bin/leon; cp fndelt ../../bin/leon; \
	cp generate ../../bin/leon; cp inter ../../bin/leon; \
	cp orbdes ../../bin/leon; cp orblist ../../bin/leon; \
	cp randobj ../../bin/leon; cp setstab ../../bin/leon; \
	cp wtdist ../../bin/leon; cp src/*.sh ../../bin/leon; \
	cp wtdist ../../bin; cp desauto ../../bin; \
	cp wtdist ../../bin/x86_64-unknown-linux-gnu-gcc; cp desauto ../../bin/x86_64-unknown-linux-gnu-gcc \
	)
# the last two for backwards compatibility?

all2:	  leonconv minimum-weight

# rules to make the executables, just link them together
leonconv: leonconv.o
	$(CC) $(CFLAGS) -o leonconv leonconv.o

minimum-weight: minimum-weight.o minimum-weight-gf2.o minimum-weight-gf3.o popcount.o
	$(CC) -lm -o minimum-weight \
		minimum-weight.o minimum-weight-gf2.o minimum-weight-gf3.o popcount.o
	
# rules to make the .o files, just compile the .c file
# cannot use implicit rule, because .c files are in a different directory
leonconv.o: ../../src/leonconv.c
	$(CC) -c $(CFLAGS) -o leonconv.o -c ../../src/leonconv.c

minimum-weight.o: $(CJSRCDIR)/minimum-weight.c $(CJSRCDIR)/minimum-weight-gf2.h $(CJSRCDIR)/minimum-weight-gf3.h $(CJSRCDIR)/popcount.h $(CJSRCDIR)/config.h $(CJSRCDIR)/types.h
	$(CC) -c -O3 -Wall -I $(CJSRCDIR) $(CJSRCDIR)/minimum-weight.c
minimum-weight-gf2.o: $(CJSRCDIR)/minimum-weight-gf2.c $(CJSRCDIR)/minimum-weight-gf2.h $(CJSRCDIR)/popcount.h $(CJSRCDIR)/config.h $(CJSRCDIR)/types.h
	$(CC) -c -O3 -Wall -I $(CJSRCDIR) $(CJSRCDIR)/minimum-weight-gf2.c
minimum-weight-gf3.o: $(CJSRCDIR)/minimum-weight-gf3.c $(CJSRCDIR)/minimum-weight-gf3.h $(CJSRCDIR)/popcount.h $(CJSRCDIR)/config.h $(CJSRCDIR)/types.h
	$(CC) -c -O3 -Wall -I $(CJSRCDIR) $(CJSRCDIR)/minimum-weight-gf3.c
popcount.o: $(CJSRCDIR)/popcount.c $(CJSRCDIR)/popcount.h $(CJSRCDIR)/config.h $(CJSRCDIR)/types.h
	$(CC) -c -O3 -Wall -I $(CJSRCDIR) $(CJSRCDIR)/popcount.c

# pseudo targets
clean:
	( cd bin/x86_64-unknown-linux-gnu-gcc; rm -f *.o )
	( cd src && make clean )
	( cd src/leon && make clean )

distclean: clean
	( rm -rf bin )
	( rm -f Makefile )
	( cd src && make distclean )
	( cd src/leon && make distclean )

# for GAP distribution
src_dist:
	@(cmp ${PKG_PATH}/guava/doc/guava.tex \
         ${GAP_PATH}/doc/guava.tex \
         || echo \
         "*** WARNING: current 'guava.tex' and 'doc/guava.tex' differ ***")
	@zoo ah ${SRCDISTFILE}.zoo \
                ${PKG_PATH}/guava/Makefile \
                ${PKG_PATH}/guava/doc/guava.tex \
                ${PKG_PATH}/guava/init.g \
                `find ${PKG_PATH}/guava/lib -name "*.g" -print` \
                `find ${PKG_PATH}/guava/tbl -name "*.g" -print` \
                `find ${PKG_PATH}/guava/src -print`
	@zoo PE ${SRCDISTFILE}.zoo

