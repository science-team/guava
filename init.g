#############################################################################
##
#A  init.g                  GUAVA library                       Reinald Baart
#A                                                         Jasper Cramwinckel
#A                                                            Erik Roijackers
#A                                                                Eric Minkes
#A                                                                 Lea Ruscio
#A                                                               David Joyner
#A                                                                  CJ, Tjhai
##
#H  @(#)$Id: init.g,v 2.0 2003/02/27 22:45:16 gap Exp $
## added read curves.gd 5-2005
## added existence check for minimum-weight 
##

##
##  Announce the package version and test for the existence of the binary.
##
DeclarePackage( "guava", "3.6",
  function()
    local path;

    if not CompareVersionNumbers( VERSION, "4.4.5" ) then
        Info( InfoWarning, 1,
              "Package ``GUAVA'': requires at least GAP 4.4.5" );
        return fail;
    fi;

    # Test for existence of the compiled binary
    path := DirectoriesPackagePrograms( "guava" );

    if ForAny( ["desauto", "leonconv", "wtdist", "minimum-weight"], 
               f -> Filename( path, f ) = fail ) then
        Info( InfoWarning, 1,
              "Package ``GUAVA'': the C code programs are not compiled." );
        Info( InfoWarning, 1,
              "Some GUAVA functions, e.g. `ConstantWeightSubcode' and MinimumWeight, ",
              "will be unavailable. ");
        Info( InfoWarning, 1,
              "See ?Installing GUAVA" );
    fi;

    return true;
  end );

DeclarePackageAutoDocumentation( "GUAVA", "doc", "GUAVA",
                                 "GUAVA Coding Theory Package" );

ReadPkg("guava", "lib/util2.gd"); 
ReadPkg("guava", "lib/codeword.gd");   
ReadPkg("guava", "lib/codegen.gd"); 
ReadPkg("guava", "lib/matrices.gd");
ReadPkg("guava", "lib/codeman.gd"); 
ReadPkg("guava", "lib/nordrob.gd"); 
ReadPkg("guava", "lib/util.gd"); 
ReadPkg("guava", "lib/curves.gd"); 
ReadPkg("guava", "lib/codeops.gd"); 
ReadPkg("guava", "lib/bounds.gd"); 
ReadPkg("guava", "lib/codefun.gd"); 
ReadPkg("guava", "lib/decoders.gd"); 
ReadPkg("guava", "lib/codecr.gd");
ReadPkg("guava", "lib/codecstr.gd");
ReadPkg("guava", "lib/codemisc.gd");
ReadPkg("guava", "lib/codenorm.gd");
ReadPkg("guava", "lib/tblgener.gd"); 
ReadPkg("guava", "lib/toric.gd"); 

