\contentsline {chapter}{\numberline {1}\leavevmode {\color {Chapter }Introduction}}{12}{chapter.1}
\contentsline {section}{\numberline {1.1}\leavevmode {\color {Chapter }Introduction to the \textsf {GUAVA} package}}{12}{section.1.1}
\contentsline {section}{\numberline {1.2}\leavevmode {\color {Chapter }Installing \textsf {GUAVA}}}{12}{section.1.2}
\contentsline {section}{\numberline {1.3}\leavevmode {\color {Chapter }Loading \textsf {GUAVA}}}{13}{section.1.3}
\contentsline {chapter}{\numberline {2}\leavevmode {\color {Chapter }Coding theory functions in GAP}}{14}{chapter.2}
\contentsline {section}{\numberline {2.1}\leavevmode {\color {Chapter } Distance functions }}{14}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}\leavevmode {\color {Chapter }AClosestVectorCombinationsMatFFEVecFFE}}{14}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}\leavevmode {\color {Chapter }AClosestVectorComb..MatFFEVecFFECoords}}{15}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}\leavevmode {\color {Chapter }DistancesDistributionMatFFEVecFFE}}{15}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}\leavevmode {\color {Chapter }DistancesDistributionVecFFEsVecFFE}}{16}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}\leavevmode {\color {Chapter }WeightVecFFE}}{16}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}\leavevmode {\color {Chapter }DistanceVecFFE}}{16}{subsection.2.1.6}
\contentsline {section}{\numberline {2.2}\leavevmode {\color {Chapter } Other functions }}{17}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}\leavevmode {\color {Chapter }ConwayPolynomial}}{17}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}\leavevmode {\color {Chapter }RandomPrimitivePolynomial}}{18}{subsection.2.2.2}
\contentsline {chapter}{\numberline {3}\leavevmode {\color {Chapter }Codewords}}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}\leavevmode {\color {Chapter }Construction of Codewords}}{20}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}\leavevmode {\color {Chapter }Codeword}}{20}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}\leavevmode {\color {Chapter }CodewordNr}}{21}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}\leavevmode {\color {Chapter }IsCodeword}}{22}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}\leavevmode {\color {Chapter }Comparisons of Codewords}}{22}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}\leavevmode {\color {Chapter }=}}{22}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}\leavevmode {\color {Chapter }Arithmetic Operations for Codewords}}{23}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}\leavevmode {\color {Chapter }+}}{23}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}\leavevmode {\color {Chapter }-}}{23}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}\leavevmode {\color {Chapter }+}}{23}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}\leavevmode {\color {Chapter } Functions that Convert Codewords to Vectors or Polynomials }}{24}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}\leavevmode {\color {Chapter }VectorCodeword}}{24}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}\leavevmode {\color {Chapter }PolyCodeword}}{24}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}\leavevmode {\color {Chapter } Functions that Change the Display Form of a Codeword }}{25}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}\leavevmode {\color {Chapter }TreatAsVector}}{25}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}\leavevmode {\color {Chapter }TreatAsPoly}}{25}{subsection.3.5.2}
\contentsline {section}{\numberline {3.6}\leavevmode {\color {Chapter } Other Codeword Functions }}{26}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}\leavevmode {\color {Chapter }NullWord}}{26}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}\leavevmode {\color {Chapter }DistanceCodeword}}{26}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}\leavevmode {\color {Chapter }Support}}{26}{subsection.3.6.3}
\contentsline {subsection}{\numberline {3.6.4}\leavevmode {\color {Chapter }WeightCodeword}}{27}{subsection.3.6.4}
\contentsline {chapter}{\numberline {4}\leavevmode {\color {Chapter }Codes}}{28}{chapter.4}
\contentsline {section}{\numberline {4.1}\leavevmode {\color {Chapter }Comparisons of Codes}}{30}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}\leavevmode {\color {Chapter }=}}{30}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}\leavevmode {\color {Chapter } Operations for Codes }}{31}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}\leavevmode {\color {Chapter }+}}{31}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}\leavevmode {\color {Chapter }*}}{31}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}\leavevmode {\color {Chapter }*}}{31}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}\leavevmode {\color {Chapter }InformationWord}}{32}{subsection.4.2.4}
\contentsline {section}{\numberline {4.3}\leavevmode {\color {Chapter } Boolean Functions for Codes }}{32}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}\leavevmode {\color {Chapter }in}}{32}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}\leavevmode {\color {Chapter }IsSubset}}{33}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}\leavevmode {\color {Chapter }IsCode}}{33}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}\leavevmode {\color {Chapter }IsLinearCode}}{33}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}\leavevmode {\color {Chapter }IsCyclicCode}}{33}{subsection.4.3.5}
\contentsline {subsection}{\numberline {4.3.6}\leavevmode {\color {Chapter }IsPerfectCode}}{34}{subsection.4.3.6}
\contentsline {subsection}{\numberline {4.3.7}\leavevmode {\color {Chapter }IsMDSCode}}{34}{subsection.4.3.7}
\contentsline {subsection}{\numberline {4.3.8}\leavevmode {\color {Chapter }IsSelfDualCode}}{35}{subsection.4.3.8}
\contentsline {subsection}{\numberline {4.3.9}\leavevmode {\color {Chapter }IsSelfOrthogonalCode}}{35}{subsection.4.3.9}
\contentsline {subsection}{\numberline {4.3.10}\leavevmode {\color {Chapter }IsDoublyEvenCode}}{35}{subsection.4.3.10}
\contentsline {subsection}{\numberline {4.3.11}\leavevmode {\color {Chapter }IsSinglyEvenCode}}{36}{subsection.4.3.11}
\contentsline {subsection}{\numberline {4.3.12}\leavevmode {\color {Chapter }IsEvenCode}}{36}{subsection.4.3.12}
\contentsline {subsection}{\numberline {4.3.13}\leavevmode {\color {Chapter }IsSelfComplementaryCode}}{37}{subsection.4.3.13}
\contentsline {subsection}{\numberline {4.3.14}\leavevmode {\color {Chapter }IsAffineCode}}{37}{subsection.4.3.14}
\contentsline {subsection}{\numberline {4.3.15}\leavevmode {\color {Chapter }IsAlmostAffineCode}}{38}{subsection.4.3.15}
\contentsline {section}{\numberline {4.4}\leavevmode {\color {Chapter } Equivalence and Isomorphism of Codes }}{38}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}\leavevmode {\color {Chapter }IsEquivalent}}{38}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}\leavevmode {\color {Chapter }CodeIsomorphism}}{38}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}\leavevmode {\color {Chapter }AutomorphismGroup}}{39}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}\leavevmode {\color {Chapter }PermutationAutomorphismGroup}}{40}{subsection.4.4.4}
\contentsline {section}{\numberline {4.5}\leavevmode {\color {Chapter } Domain Functions for Codes }}{40}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}\leavevmode {\color {Chapter }IsFinite}}{40}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}\leavevmode {\color {Chapter }Size}}{40}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}\leavevmode {\color {Chapter }LeftActingDomain}}{41}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}\leavevmode {\color {Chapter }Dimension}}{41}{subsection.4.5.4}
\contentsline {subsection}{\numberline {4.5.5}\leavevmode {\color {Chapter }AsSSortedList}}{41}{subsection.4.5.5}
\contentsline {section}{\numberline {4.6}\leavevmode {\color {Chapter } Printing and Displaying Codes }}{42}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}\leavevmode {\color {Chapter }Print}}{42}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}\leavevmode {\color {Chapter }String}}{42}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}\leavevmode {\color {Chapter }Display}}{43}{subsection.4.6.3}
\contentsline {subsection}{\numberline {4.6.4}\leavevmode {\color {Chapter }DisplayBoundsInfo}}{43}{subsection.4.6.4}
\contentsline {section}{\numberline {4.7}\leavevmode {\color {Chapter } Generating (Check) Matrices and Polynomials }}{44}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}\leavevmode {\color {Chapter }GeneratorMat}}{44}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}\leavevmode {\color {Chapter }CheckMat}}{44}{subsection.4.7.2}
\contentsline {subsection}{\numberline {4.7.3}\leavevmode {\color {Chapter }GeneratorPol}}{45}{subsection.4.7.3}
\contentsline {subsection}{\numberline {4.7.4}\leavevmode {\color {Chapter }CheckPol}}{45}{subsection.4.7.4}
\contentsline {subsection}{\numberline {4.7.5}\leavevmode {\color {Chapter }RootsOfCode}}{45}{subsection.4.7.5}
\contentsline {section}{\numberline {4.8}\leavevmode {\color {Chapter } Parameters of Codes }}{46}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}\leavevmode {\color {Chapter }WordLength}}{46}{subsection.4.8.1}
\contentsline {subsection}{\numberline {4.8.2}\leavevmode {\color {Chapter }Redundancy}}{46}{subsection.4.8.2}
\contentsline {subsection}{\numberline {4.8.3}\leavevmode {\color {Chapter }MinimumDistance}}{46}{subsection.4.8.3}
\contentsline {subsection}{\numberline {4.8.4}\leavevmode {\color {Chapter }MinimumDistanceLeon}}{47}{subsection.4.8.4}
\contentsline {subsection}{\numberline {4.8.5}\leavevmode {\color {Chapter }MinimumWeight}}{48}{subsection.4.8.5}
\contentsline {subsection}{\numberline {4.8.6}\leavevmode {\color {Chapter }DecreaseMinimumDistanceUpperBound}}{51}{subsection.4.8.6}
\contentsline {subsection}{\numberline {4.8.7}\leavevmode {\color {Chapter }MinimumDistanceRandom}}{51}{subsection.4.8.7}
\contentsline {subsection}{\numberline {4.8.8}\leavevmode {\color {Chapter }CoveringRadius}}{53}{subsection.4.8.8}
\contentsline {subsection}{\numberline {4.8.9}\leavevmode {\color {Chapter }SetCoveringRadius}}{54}{subsection.4.8.9}
\contentsline {section}{\numberline {4.9}\leavevmode {\color {Chapter } Distributions }}{54}{section.4.9}
\contentsline {subsection}{\numberline {4.9.1}\leavevmode {\color {Chapter }MinimumWeightWords}}{54}{subsection.4.9.1}
\contentsline {subsection}{\numberline {4.9.2}\leavevmode {\color {Chapter }WeightDistribution}}{54}{subsection.4.9.2}
\contentsline {subsection}{\numberline {4.9.3}\leavevmode {\color {Chapter }InnerDistribution}}{55}{subsection.4.9.3}
\contentsline {subsection}{\numberline {4.9.4}\leavevmode {\color {Chapter }DistancesDistribution}}{55}{subsection.4.9.4}
\contentsline {subsection}{\numberline {4.9.5}\leavevmode {\color {Chapter }OuterDistribution}}{56}{subsection.4.9.5}
\contentsline {section}{\numberline {4.10}\leavevmode {\color {Chapter } Decoding Functions }}{56}{section.4.10}
\contentsline {subsection}{\numberline {4.10.1}\leavevmode {\color {Chapter }Decode}}{56}{subsection.4.10.1}
\contentsline {subsection}{\numberline {4.10.2}\leavevmode {\color {Chapter }Decodeword}}{57}{subsection.4.10.2}
\contentsline {subsection}{\numberline {4.10.3}\leavevmode {\color {Chapter }GeneralizedReedSolomonDecoderGao}}{58}{subsection.4.10.3}
\contentsline {subsection}{\numberline {4.10.4}\leavevmode {\color {Chapter }GeneralizedReedSolomonListDecoder}}{58}{subsection.4.10.4}
\contentsline {subsection}{\numberline {4.10.5}\leavevmode {\color {Chapter }BitFlipDecoder}}{59}{subsection.4.10.5}
\contentsline {subsection}{\numberline {4.10.6}\leavevmode {\color {Chapter }NearestNeighborGRSDecodewords}}{60}{subsection.4.10.6}
\contentsline {subsection}{\numberline {4.10.7}\leavevmode {\color {Chapter }NearestNeighborDecodewords}}{60}{subsection.4.10.7}
\contentsline {subsection}{\numberline {4.10.8}\leavevmode {\color {Chapter }Syndrome}}{61}{subsection.4.10.8}
\contentsline {subsection}{\numberline {4.10.9}\leavevmode {\color {Chapter }SyndromeTable}}{62}{subsection.4.10.9}
\contentsline {subsection}{\numberline {4.10.10}\leavevmode {\color {Chapter }StandardArray}}{62}{subsection.4.10.10}
\contentsline {subsection}{\numberline {4.10.11}\leavevmode {\color {Chapter }PermutationDecode}}{62}{subsection.4.10.11}
\contentsline {subsection}{\numberline {4.10.12}\leavevmode {\color {Chapter }PermutationDecodeNC}}{63}{subsection.4.10.12}
\contentsline {chapter}{\numberline {5}\leavevmode {\color {Chapter }Generating Codes}}{64}{chapter.5}
\contentsline {section}{\numberline {5.1}\leavevmode {\color {Chapter } Generating Unrestricted Codes }}{64}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}\leavevmode {\color {Chapter }ElementsCode}}{64}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}\leavevmode {\color {Chapter }HadamardCode}}{65}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}\leavevmode {\color {Chapter }ConferenceCode}}{65}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}\leavevmode {\color {Chapter }MOLSCode}}{66}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}\leavevmode {\color {Chapter }RandomCode}}{67}{subsection.5.1.5}
\contentsline {subsection}{\numberline {5.1.6}\leavevmode {\color {Chapter }NordstromRobinsonCode}}{67}{subsection.5.1.6}
\contentsline {subsection}{\numberline {5.1.7}\leavevmode {\color {Chapter }GreedyCode}}{67}{subsection.5.1.7}
\contentsline {subsection}{\numberline {5.1.8}\leavevmode {\color {Chapter }LexiCode}}{68}{subsection.5.1.8}
\contentsline {section}{\numberline {5.2}\leavevmode {\color {Chapter } Generating Linear Codes }}{68}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}\leavevmode {\color {Chapter }GeneratorMatCode}}{68}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}\leavevmode {\color {Chapter }CheckMatCodeMutable}}{69}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}\leavevmode {\color {Chapter }CheckMatCode}}{69}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}\leavevmode {\color {Chapter }HammingCode}}{70}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}\leavevmode {\color {Chapter }ReedMullerCode}}{70}{subsection.5.2.5}
\contentsline {subsection}{\numberline {5.2.6}\leavevmode {\color {Chapter }AlternantCode}}{70}{subsection.5.2.6}
\contentsline {subsection}{\numberline {5.2.7}\leavevmode {\color {Chapter }GoppaCode}}{71}{subsection.5.2.7}
\contentsline {subsection}{\numberline {5.2.8}\leavevmode {\color {Chapter }GeneralizedSrivastavaCode}}{71}{subsection.5.2.8}
\contentsline {subsection}{\numberline {5.2.9}\leavevmode {\color {Chapter }SrivastavaCode}}{72}{subsection.5.2.9}
\contentsline {subsection}{\numberline {5.2.10}\leavevmode {\color {Chapter }CordaroWagnerCode}}{72}{subsection.5.2.10}
\contentsline {subsection}{\numberline {5.2.11}\leavevmode {\color {Chapter }FerreroDesignCode}}{72}{subsection.5.2.11}
\contentsline {subsection}{\numberline {5.2.12}\leavevmode {\color {Chapter }RandomLinearCode}}{73}{subsection.5.2.12}
\contentsline {subsection}{\numberline {5.2.13}\leavevmode {\color {Chapter }OptimalityCode}}{74}{subsection.5.2.13}
\contentsline {subsection}{\numberline {5.2.14}\leavevmode {\color {Chapter }BestKnownLinearCode}}{74}{subsection.5.2.14}
\contentsline {section}{\numberline {5.3}\leavevmode {\color {Chapter } Gabidulin Codes }}{76}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}\leavevmode {\color {Chapter }GabidulinCode}}{76}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}\leavevmode {\color {Chapter }EnlargedGabidulinCode}}{76}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}\leavevmode {\color {Chapter }DavydovCode}}{76}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}\leavevmode {\color {Chapter }TombakCode}}{76}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}\leavevmode {\color {Chapter }EnlargedTombakCode}}{76}{subsection.5.3.5}
\contentsline {section}{\numberline {5.4}\leavevmode {\color {Chapter } Golay Codes }}{77}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}\leavevmode {\color {Chapter }BinaryGolayCode}}{77}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}\leavevmode {\color {Chapter }ExtendedBinaryGolayCode}}{77}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}\leavevmode {\color {Chapter }TernaryGolayCode}}{78}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}\leavevmode {\color {Chapter }ExtendedTernaryGolayCode}}{78}{subsection.5.4.4}
\contentsline {section}{\numberline {5.5}\leavevmode {\color {Chapter } Generating Cyclic Codes }}{78}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}\leavevmode {\color {Chapter }GeneratorPolCode}}{79}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}\leavevmode {\color {Chapter }CheckPolCode}}{80}{subsection.5.5.2}
\contentsline {subsection}{\numberline {5.5.3}\leavevmode {\color {Chapter }RootsCode}}{80}{subsection.5.5.3}
\contentsline {subsection}{\numberline {5.5.4}\leavevmode {\color {Chapter }BCHCode}}{81}{subsection.5.5.4}
\contentsline {subsection}{\numberline {5.5.5}\leavevmode {\color {Chapter }ReedSolomonCode}}{82}{subsection.5.5.5}
\contentsline {subsection}{\numberline {5.5.6}\leavevmode {\color {Chapter }ExtendedReedSolomonCode}}{82}{subsection.5.5.6}
\contentsline {subsection}{\numberline {5.5.7}\leavevmode {\color {Chapter }QRCode}}{82}{subsection.5.5.7}
\contentsline {subsection}{\numberline {5.5.8}\leavevmode {\color {Chapter }QQRCodeNC}}{83}{subsection.5.5.8}
\contentsline {subsection}{\numberline {5.5.9}\leavevmode {\color {Chapter }QQRCode}}{83}{subsection.5.5.9}
\contentsline {subsection}{\numberline {5.5.10}\leavevmode {\color {Chapter }FireCode}}{84}{subsection.5.5.10}
\contentsline {subsection}{\numberline {5.5.11}\leavevmode {\color {Chapter }WholeSpaceCode}}{84}{subsection.5.5.11}
\contentsline {subsection}{\numberline {5.5.12}\leavevmode {\color {Chapter }NullCode}}{85}{subsection.5.5.12}
\contentsline {subsection}{\numberline {5.5.13}\leavevmode {\color {Chapter }RepetitionCode}}{85}{subsection.5.5.13}
\contentsline {subsection}{\numberline {5.5.14}\leavevmode {\color {Chapter }CyclicCodes}}{85}{subsection.5.5.14}
\contentsline {subsection}{\numberline {5.5.15}\leavevmode {\color {Chapter }NrCyclicCodes}}{85}{subsection.5.5.15}
\contentsline {subsection}{\numberline {5.5.16}\leavevmode {\color {Chapter }QuasiCyclicCode}}{86}{subsection.5.5.16}
\contentsline {subsection}{\numberline {5.5.17}\leavevmode {\color {Chapter }CyclicMDSCode}}{87}{subsection.5.5.17}
\contentsline {subsection}{\numberline {5.5.18}\leavevmode {\color {Chapter }FourNegacirculantSelfDualCode}}{88}{subsection.5.5.18}
\contentsline {subsection}{\numberline {5.5.19}\leavevmode {\color {Chapter }FourNegacirculantSelfDualCodeNC}}{89}{subsection.5.5.19}
\contentsline {section}{\numberline {5.6}\leavevmode {\color {Chapter } Evaluation Codes }}{89}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}\leavevmode {\color {Chapter }EvaluationCode}}{89}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}\leavevmode {\color {Chapter }GeneralizedReedSolomonCode}}{89}{subsection.5.6.2}
\contentsline {subsection}{\numberline {5.6.3}\leavevmode {\color {Chapter }GeneralizedReedMullerCode}}{90}{subsection.5.6.3}
\contentsline {subsection}{\numberline {5.6.4}\leavevmode {\color {Chapter }ToricPoints}}{91}{subsection.5.6.4}
\contentsline {subsection}{\numberline {5.6.5}\leavevmode {\color {Chapter }ToricCode}}{91}{subsection.5.6.5}
\contentsline {section}{\numberline {5.7}\leavevmode {\color {Chapter } Algebraic geometric codes }}{92}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}\leavevmode {\color {Chapter }AffineCurve}}{92}{subsection.5.7.1}
\contentsline {subsection}{\numberline {5.7.2}\leavevmode {\color {Chapter }AffinePointsOnCurve}}{93}{subsection.5.7.2}
\contentsline {subsection}{\numberline {5.7.3}\leavevmode {\color {Chapter }GenusCurve}}{93}{subsection.5.7.3}
\contentsline {subsection}{\numberline {5.7.4}\leavevmode {\color {Chapter }GOrbitPoint }}{93}{subsection.5.7.4}
\contentsline {subsection}{\numberline {5.7.5}\leavevmode {\color {Chapter }DivisorOnAffineCurve}}{95}{subsection.5.7.5}
\contentsline {subsection}{\numberline {5.7.6}\leavevmode {\color {Chapter }DivisorAddition }}{95}{subsection.5.7.6}
\contentsline {subsection}{\numberline {5.7.7}\leavevmode {\color {Chapter }DivisorDegree }}{95}{subsection.5.7.7}
\contentsline {subsection}{\numberline {5.7.8}\leavevmode {\color {Chapter }DivisorNegate }}{96}{subsection.5.7.8}
\contentsline {subsection}{\numberline {5.7.9}\leavevmode {\color {Chapter }DivisorIsZero }}{96}{subsection.5.7.9}
\contentsline {subsection}{\numberline {5.7.10}\leavevmode {\color {Chapter }DivisorsEqual }}{96}{subsection.5.7.10}
\contentsline {subsection}{\numberline {5.7.11}\leavevmode {\color {Chapter }DivisorGCD }}{96}{subsection.5.7.11}
\contentsline {subsection}{\numberline {5.7.12}\leavevmode {\color {Chapter }DivisorLCM }}{96}{subsection.5.7.12}
\contentsline {subsection}{\numberline {5.7.13}\leavevmode {\color {Chapter }RiemannRochSpaceBasisFunctionP1 }}{98}{subsection.5.7.13}
\contentsline {subsection}{\numberline {5.7.14}\leavevmode {\color {Chapter }DivisorOfRationalFunctionP1 }}{98}{subsection.5.7.14}
\contentsline {subsection}{\numberline {5.7.15}\leavevmode {\color {Chapter }RiemannRochSpaceBasisP1 }}{99}{subsection.5.7.15}
\contentsline {subsection}{\numberline {5.7.16}\leavevmode {\color {Chapter }MoebiusTransformation }}{100}{subsection.5.7.16}
\contentsline {subsection}{\numberline {5.7.17}\leavevmode {\color {Chapter }ActionMoebiusTransformationOnFunction }}{100}{subsection.5.7.17}
\contentsline {subsection}{\numberline {5.7.18}\leavevmode {\color {Chapter }ActionMoebiusTransformationOnDivisorP1 }}{100}{subsection.5.7.18}
\contentsline {subsection}{\numberline {5.7.19}\leavevmode {\color {Chapter }IsActionMoebiusTransformationOnDivisorDefinedP1 }}{100}{subsection.5.7.19}
\contentsline {subsection}{\numberline {5.7.20}\leavevmode {\color {Chapter }DivisorAutomorphismGroupP1 }}{101}{subsection.5.7.20}
\contentsline {subsection}{\numberline {5.7.21}\leavevmode {\color {Chapter }MatrixRepresentationOnRiemannRochSpaceP1 }}{102}{subsection.5.7.21}
\contentsline {subsection}{\numberline {5.7.22}\leavevmode {\color {Chapter }GoppaCodeClassical}}{103}{subsection.5.7.22}
\contentsline {subsection}{\numberline {5.7.23}\leavevmode {\color {Chapter }EvaluationBivariateCode}}{103}{subsection.5.7.23}
\contentsline {subsection}{\numberline {5.7.24}\leavevmode {\color {Chapter }EvaluationBivariateCodeNC}}{103}{subsection.5.7.24}
\contentsline {subsection}{\numberline {5.7.25}\leavevmode {\color {Chapter }OnePointAGCode}}{104}{subsection.5.7.25}
\contentsline {section}{\numberline {5.8}\leavevmode {\color {Chapter } Low-Density Parity-Check Codes }}{105}{section.5.8}
\contentsline {subsection}{\numberline {5.8.1}\leavevmode {\color {Chapter }QCLDPCCodeFromGroup}}{106}{subsection.5.8.1}
\contentsline {chapter}{\numberline {6}\leavevmode {\color {Chapter }Manipulating Codes}}{108}{chapter.6}
\contentsline {section}{\numberline {6.1}\leavevmode {\color {Chapter } Functions that Generate a New Code from a Given Code }}{108}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}\leavevmode {\color {Chapter }ExtendedCode}}{108}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}\leavevmode {\color {Chapter }PuncturedCode}}{109}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}\leavevmode {\color {Chapter }EvenWeightSubcode}}{109}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}\leavevmode {\color {Chapter }PermutedCode}}{110}{subsection.6.1.4}
\contentsline {subsection}{\numberline {6.1.5}\leavevmode {\color {Chapter }ExpurgatedCode}}{110}{subsection.6.1.5}
\contentsline {subsection}{\numberline {6.1.6}\leavevmode {\color {Chapter }AugmentedCode}}{111}{subsection.6.1.6}
\contentsline {subsection}{\numberline {6.1.7}\leavevmode {\color {Chapter }RemovedElementsCode}}{111}{subsection.6.1.7}
\contentsline {subsection}{\numberline {6.1.8}\leavevmode {\color {Chapter }AddedElementsCode}}{112}{subsection.6.1.8}
\contentsline {subsection}{\numberline {6.1.9}\leavevmode {\color {Chapter }ShortenedCode}}{112}{subsection.6.1.9}
\contentsline {subsection}{\numberline {6.1.10}\leavevmode {\color {Chapter }LengthenedCode}}{113}{subsection.6.1.10}
\contentsline {subsection}{\numberline {6.1.11}\leavevmode {\color {Chapter }SubCode}}{114}{subsection.6.1.11}
\contentsline {subsection}{\numberline {6.1.12}\leavevmode {\color {Chapter }ResidueCode}}{114}{subsection.6.1.12}
\contentsline {subsection}{\numberline {6.1.13}\leavevmode {\color {Chapter }ConstructionBCode}}{114}{subsection.6.1.13}
\contentsline {subsection}{\numberline {6.1.14}\leavevmode {\color {Chapter }DualCode}}{115}{subsection.6.1.14}
\contentsline {subsection}{\numberline {6.1.15}\leavevmode {\color {Chapter }ConversionFieldCode}}{116}{subsection.6.1.15}
\contentsline {subsection}{\numberline {6.1.16}\leavevmode {\color {Chapter }TraceCode}}{116}{subsection.6.1.16}
\contentsline {subsection}{\numberline {6.1.17}\leavevmode {\color {Chapter }CosetCode}}{116}{subsection.6.1.17}
\contentsline {subsection}{\numberline {6.1.18}\leavevmode {\color {Chapter }ConstantWeightSubcode}}{117}{subsection.6.1.18}
\contentsline {subsection}{\numberline {6.1.19}\leavevmode {\color {Chapter }StandardFormCode}}{117}{subsection.6.1.19}
\contentsline {subsection}{\numberline {6.1.20}\leavevmode {\color {Chapter }PiecewiseConstantCode}}{118}{subsection.6.1.20}
\contentsline {section}{\numberline {6.2}\leavevmode {\color {Chapter } Functions that Generate a New Code from Two or More Given Codes }}{119}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}\leavevmode {\color {Chapter }DirectSumCode}}{119}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}\leavevmode {\color {Chapter }UUVCode}}{119}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}\leavevmode {\color {Chapter }DirectProductCode}}{119}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}\leavevmode {\color {Chapter }IntersectionCode}}{120}{subsection.6.2.4}
\contentsline {subsection}{\numberline {6.2.5}\leavevmode {\color {Chapter }UnionCode}}{120}{subsection.6.2.5}
\contentsline {subsection}{\numberline {6.2.6}\leavevmode {\color {Chapter }ExtendedDirectSumCode}}{121}{subsection.6.2.6}
\contentsline {subsection}{\numberline {6.2.7}\leavevmode {\color {Chapter }AmalgamatedDirectSumCode}}{121}{subsection.6.2.7}
\contentsline {subsection}{\numberline {6.2.8}\leavevmode {\color {Chapter }BlockwiseDirectSumCode}}{122}{subsection.6.2.8}
\contentsline {subsection}{\numberline {6.2.9}\leavevmode {\color {Chapter }ConstructionXCode}}{122}{subsection.6.2.9}
\contentsline {subsection}{\numberline {6.2.10}\leavevmode {\color {Chapter }ConstructionXXCode}}{123}{subsection.6.2.10}
\contentsline {subsection}{\numberline {6.2.11}\leavevmode {\color {Chapter }BZCode}}{124}{subsection.6.2.11}
\contentsline {subsection}{\numberline {6.2.12}\leavevmode {\color {Chapter }BZCodeNC}}{125}{subsection.6.2.12}
\contentsline {chapter}{\numberline {7}\leavevmode {\color {Chapter } Bounds on codes, special matrices and miscellaneous functions }}{126}{chapter.7}
\contentsline {section}{\numberline {7.1}\leavevmode {\color {Chapter } Distance bounds on codes }}{126}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}\leavevmode {\color {Chapter }UpperBoundSingleton}}{127}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}\leavevmode {\color {Chapter }UpperBoundHamming}}{127}{subsection.7.1.2}
\contentsline {subsection}{\numberline {7.1.3}\leavevmode {\color {Chapter }UpperBoundJohnson}}{127}{subsection.7.1.3}
\contentsline {subsection}{\numberline {7.1.4}\leavevmode {\color {Chapter }UpperBoundPlotkin}}{128}{subsection.7.1.4}
\contentsline {subsection}{\numberline {7.1.5}\leavevmode {\color {Chapter }UpperBoundElias}}{128}{subsection.7.1.5}
\contentsline {subsection}{\numberline {7.1.6}\leavevmode {\color {Chapter }UpperBoundGriesmer}}{129}{subsection.7.1.6}
\contentsline {subsection}{\numberline {7.1.7}\leavevmode {\color {Chapter }IsGriesmerCode}}{129}{subsection.7.1.7}
\contentsline {subsection}{\numberline {7.1.8}\leavevmode {\color {Chapter }UpperBound}}{129}{subsection.7.1.8}
\contentsline {subsection}{\numberline {7.1.9}\leavevmode {\color {Chapter }LowerBoundMinimumDistance}}{130}{subsection.7.1.9}
\contentsline {subsection}{\numberline {7.1.10}\leavevmode {\color {Chapter }LowerBoundGilbertVarshamov}}{130}{subsection.7.1.10}
\contentsline {subsection}{\numberline {7.1.11}\leavevmode {\color {Chapter }LowerBoundSpherePacking}}{130}{subsection.7.1.11}
\contentsline {subsection}{\numberline {7.1.12}\leavevmode {\color {Chapter }UpperBoundMinimumDistance}}{131}{subsection.7.1.12}
\contentsline {subsection}{\numberline {7.1.13}\leavevmode {\color {Chapter }BoundsMinimumDistance}}{131}{subsection.7.1.13}
\contentsline {section}{\numberline {7.2}\leavevmode {\color {Chapter } Covering radius bounds on codes }}{132}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}\leavevmode {\color {Chapter }BoundsCoveringRadius}}{132}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}\leavevmode {\color {Chapter }IncreaseCoveringRadiusLowerBound}}{132}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}\leavevmode {\color {Chapter }ExhaustiveSearchCoveringRadius}}{133}{subsection.7.2.3}
\contentsline {subsection}{\numberline {7.2.4}\leavevmode {\color {Chapter }GeneralLowerBoundCoveringRadius}}{134}{subsection.7.2.4}
\contentsline {subsection}{\numberline {7.2.5}\leavevmode {\color {Chapter }GeneralUpperBoundCoveringRadius}}{134}{subsection.7.2.5}
\contentsline {subsection}{\numberline {7.2.6}\leavevmode {\color {Chapter }LowerBoundCoveringRadiusSphereCovering}}{135}{subsection.7.2.6}
\contentsline {subsection}{\numberline {7.2.7}\leavevmode {\color {Chapter }LowerBoundCoveringRadiusVanWee1}}{135}{subsection.7.2.7}
\contentsline {subsection}{\numberline {7.2.8}\leavevmode {\color {Chapter }LowerBoundCoveringRadiusVanWee2}}{136}{subsection.7.2.8}
\contentsline {subsection}{\numberline {7.2.9}\leavevmode {\color {Chapter }LowerBoundCoveringRadiusCountingExcess}}{136}{subsection.7.2.9}
\contentsline {subsection}{\numberline {7.2.10}\leavevmode {\color {Chapter }LowerBoundCoveringRadiusEmbedded1}}{137}{subsection.7.2.10}
\contentsline {subsection}{\numberline {7.2.11}\leavevmode {\color {Chapter }LowerBoundCoveringRadiusEmbedded2}}{137}{subsection.7.2.11}
\contentsline {subsection}{\numberline {7.2.12}\leavevmode {\color {Chapter }LowerBoundCoveringRadiusInduction}}{138}{subsection.7.2.12}
\contentsline {subsection}{\numberline {7.2.13}\leavevmode {\color {Chapter }UpperBoundCoveringRadiusRedundancy}}{138}{subsection.7.2.13}
\contentsline {subsection}{\numberline {7.2.14}\leavevmode {\color {Chapter }UpperBoundCoveringRadiusDelsarte}}{139}{subsection.7.2.14}
\contentsline {subsection}{\numberline {7.2.15}\leavevmode {\color {Chapter }UpperBoundCoveringRadiusStrength}}{139}{subsection.7.2.15}
\contentsline {subsection}{\numberline {7.2.16}\leavevmode {\color {Chapter }UpperBoundCoveringRadiusGriesmerLike}}{139}{subsection.7.2.16}
\contentsline {subsection}{\numberline {7.2.17}\leavevmode {\color {Chapter }UpperBoundCoveringRadiusCyclicCode}}{140}{subsection.7.2.17}
\contentsline {section}{\numberline {7.3}\leavevmode {\color {Chapter } Special matrices in \textsf {GUAVA} }}{140}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}\leavevmode {\color {Chapter }KrawtchoukMat}}{141}{subsection.7.3.1}
\contentsline {subsection}{\numberline {7.3.2}\leavevmode {\color {Chapter }GrayMat}}{141}{subsection.7.3.2}
\contentsline {subsection}{\numberline {7.3.3}\leavevmode {\color {Chapter }SylvesterMat}}{141}{subsection.7.3.3}
\contentsline {subsection}{\numberline {7.3.4}\leavevmode {\color {Chapter }HadamardMat}}{142}{subsection.7.3.4}
\contentsline {subsection}{\numberline {7.3.5}\leavevmode {\color {Chapter }VandermondeMat}}{142}{subsection.7.3.5}
\contentsline {subsection}{\numberline {7.3.6}\leavevmode {\color {Chapter }PutStandardForm}}{143}{subsection.7.3.6}
\contentsline {subsection}{\numberline {7.3.7}\leavevmode {\color {Chapter }IsInStandardForm}}{144}{subsection.7.3.7}
\contentsline {subsection}{\numberline {7.3.8}\leavevmode {\color {Chapter }PermutedCols}}{144}{subsection.7.3.8}
\contentsline {subsection}{\numberline {7.3.9}\leavevmode {\color {Chapter }VerticalConversionFieldMat}}{144}{subsection.7.3.9}
\contentsline {subsection}{\numberline {7.3.10}\leavevmode {\color {Chapter }HorizontalConversionFieldMat}}{145}{subsection.7.3.10}
\contentsline {subsection}{\numberline {7.3.11}\leavevmode {\color {Chapter }MOLS}}{145}{subsection.7.3.11}
\contentsline {subsection}{\numberline {7.3.12}\leavevmode {\color {Chapter }IsLatinSquare}}{146}{subsection.7.3.12}
\contentsline {subsection}{\numberline {7.3.13}\leavevmode {\color {Chapter }AreMOLS}}{146}{subsection.7.3.13}
\contentsline {section}{\numberline {7.4}\leavevmode {\color {Chapter } Some functions related to the norm of a code }}{147}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}\leavevmode {\color {Chapter }CoordinateNorm}}{147}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}\leavevmode {\color {Chapter }CodeNorm}}{147}{subsection.7.4.2}
\contentsline {subsection}{\numberline {7.4.3}\leavevmode {\color {Chapter }IsCoordinateAcceptable}}{147}{subsection.7.4.3}
\contentsline {subsection}{\numberline {7.4.4}\leavevmode {\color {Chapter }GeneralizedCodeNorm}}{148}{subsection.7.4.4}
\contentsline {subsection}{\numberline {7.4.5}\leavevmode {\color {Chapter }IsNormalCode}}{148}{subsection.7.4.5}
\contentsline {section}{\numberline {7.5}\leavevmode {\color {Chapter } Miscellaneous functions }}{148}{section.7.5}
\contentsline {subsection}{\numberline {7.5.1}\leavevmode {\color {Chapter }CodeWeightEnumerator}}{148}{subsection.7.5.1}
\contentsline {subsection}{\numberline {7.5.2}\leavevmode {\color {Chapter }CodeDistanceEnumerator}}{149}{subsection.7.5.2}
\contentsline {subsection}{\numberline {7.5.3}\leavevmode {\color {Chapter }CodeMacWilliamsTransform}}{149}{subsection.7.5.3}
\contentsline {subsection}{\numberline {7.5.4}\leavevmode {\color {Chapter }CodeDensity}}{149}{subsection.7.5.4}
\contentsline {subsection}{\numberline {7.5.5}\leavevmode {\color {Chapter }SphereContent}}{150}{subsection.7.5.5}
\contentsline {subsection}{\numberline {7.5.6}\leavevmode {\color {Chapter }Krawtchouk}}{150}{subsection.7.5.6}
\contentsline {subsection}{\numberline {7.5.7}\leavevmode {\color {Chapter }PrimitiveUnityRoot}}{150}{subsection.7.5.7}
\contentsline {subsection}{\numberline {7.5.8}\leavevmode {\color {Chapter }PrimitivePolynomialsNr}}{151}{subsection.7.5.8}
\contentsline {subsection}{\numberline {7.5.9}\leavevmode {\color {Chapter }IrreduciblePolynomialsNr}}{151}{subsection.7.5.9}
\contentsline {subsection}{\numberline {7.5.10}\leavevmode {\color {Chapter }MatrixRepresentationOfElement}}{151}{subsection.7.5.10}
\contentsline {subsection}{\numberline {7.5.11}\leavevmode {\color {Chapter }ReciprocalPolynomial}}{152}{subsection.7.5.11}
\contentsline {subsection}{\numberline {7.5.12}\leavevmode {\color {Chapter }CyclotomicCosets}}{152}{subsection.7.5.12}
\contentsline {subsection}{\numberline {7.5.13}\leavevmode {\color {Chapter }WeightHistogram}}{153}{subsection.7.5.13}
\contentsline {subsection}{\numberline {7.5.14}\leavevmode {\color {Chapter }MultiplicityInList}}{153}{subsection.7.5.14}
\contentsline {subsection}{\numberline {7.5.15}\leavevmode {\color {Chapter }MostCommonInList}}{153}{subsection.7.5.15}
\contentsline {subsection}{\numberline {7.5.16}\leavevmode {\color {Chapter }RotateList}}{154}{subsection.7.5.16}
\contentsline {subsection}{\numberline {7.5.17}\leavevmode {\color {Chapter }CirculantMatrix}}{154}{subsection.7.5.17}
\contentsline {section}{\numberline {7.6}\leavevmode {\color {Chapter } Miscellaneous polynomial functions }}{154}{section.7.6}
\contentsline {subsection}{\numberline {7.6.1}\leavevmode {\color {Chapter }MatrixTransformationOnMultivariatePolynomial }}{154}{subsection.7.6.1}
\contentsline {subsection}{\numberline {7.6.2}\leavevmode {\color {Chapter }DegreeMultivariatePolynomial}}{154}{subsection.7.6.2}
\contentsline {subsection}{\numberline {7.6.3}\leavevmode {\color {Chapter }DegreesMultivariatePolynomial}}{155}{subsection.7.6.3}
\contentsline {subsection}{\numberline {7.6.4}\leavevmode {\color {Chapter }CoefficientMultivariatePolynomial}}{155}{subsection.7.6.4}
\contentsline {subsection}{\numberline {7.6.5}\leavevmode {\color {Chapter }SolveLinearSystem}}{156}{subsection.7.6.5}
\contentsline {subsection}{\numberline {7.6.6}\leavevmode {\color {Chapter }GuavaVersion}}{156}{subsection.7.6.6}
\contentsline {subsection}{\numberline {7.6.7}\leavevmode {\color {Chapter }ZechLog}}{156}{subsection.7.6.7}
\contentsline {subsection}{\numberline {7.6.8}\leavevmode {\color {Chapter }CoefficientToPolynomial}}{157}{subsection.7.6.8}
\contentsline {subsection}{\numberline {7.6.9}\leavevmode {\color {Chapter }DegreesMonomialTerm}}{157}{subsection.7.6.9}
\contentsline {subsection}{\numberline {7.6.10}\leavevmode {\color {Chapter }DivisorsMultivariatePolynomial}}{158}{subsection.7.6.10}
\contentsline {section}{\numberline {7.7}\leavevmode {\color {Chapter } GNU Free Documentation License }}{158}{section.7.7}
