                    The GUAVA Coding Theory Package
                    -------------------------------

GUAVA is a package that implements coding theory  algorithms  in  GAP.
With GUAVA, codes can be created and manipulated and information about
codes can be calculated.

GUAVA consists of various files written in the GAP  language,  and  an
external program from J.S. Leon for dealing with  automorphism  groups
of codes and isomorphism testing functions.  Several  algorithms  that
need the speed are integrated in the GAP kernel. Please send your  bug
reports to support@gap-system.org. See the section "Bug reports"
below.

GUAVA was originally written in  GAP 3  by  Jasper  Cramwinckel,  Erik
Roijackers, and Reinald Baart as a final project during their study of
Mathematics at the Delft University of Technology, Department of  Pure
Mathematics, and in Aachen, at Lehrstuhl D fuer Mathematik.

In version 1.3 (still GAP 3), new functions were added by Eric Minkes,
also from Delft University of Technology.

Version 1.4 (the first GAP 4 version) of  GUAVA  was  created  by  Lea
Ruscio who maintained it through to version 1.5.

Versions of GUAVA since 1.6 have been maintained by David Joyner.
Starting with Versions 2.5/2.6, GUAVA was forked: the "odd versions"
(e.g., 2.5) include the code of J. S. Leon mentioned above (which is not
licensed under the GPL at the time). The "even versions" (e.g., 2.6) 
were entirely GPL'd, but were otherwise are basically the same as the 
previous "odd version" (i.e., "2.6 = 2.5 - non-GPL'd code"). 

Starting with Versions 2.7/2.8, Cen Tjhai, a PhD student in the
School of Computing, Communications & Electronics at the University of 
Plymouth, was added as an author. He completely updated the tables,
which have not been updated since 1998, and added some new functions.

Starting with Versions 3.0, Robert Miller, a PhD student in the
Department of Mathematics at the University of Washington in Seattle, 
and Tom Boothby, and undergraduate in the same department, 
were added as authors. They will be revising Leon's code,
which was GPL'd on April 17th, 2007:

        I, Jeffrey S. Leon, agree to license all the partition
        backtrack code which I have written under the GPL
       (www.fsf.org) as of this date, April 17, 2007.

The ``even''/``odd'' forks of the GUAVA code are now united.

The current version is 3.6. 

                            Installing GUAVA
                            ----------------

To install GUAVA (as a GAP4 Package) unpack  the  archive  file  in  a
directory in the `pkg' hierarchy of your version of GAP4. (This  might
be the `pkg' directory of the GAP4 home directory; it is however  also
possible to  keep  an  additional  `pkg'  directory  in  your  private
directories,  see  section  "Installing  GAP  Packages"  of  the  GAP4
reference manual for details on how to do this.)

After unpacking GUAVA the GAP-only part of  GUAVA  is  installed.  The
parts of GUAVA depending on J. Leon's backtrack programs package  (for
computing  automorphism  groups)  are  only  available   in   a   UNIX
environment, where you should proceed as follows:

Go to the  newly  created  `guava'  directory  and  call  `./configure
<path>' where <path> is the path to the GAP  home  directory.  So  for
example, if you install the package in the main `pkg' directory call

  ./configure ../..

(For rpm-based 64-bit linux machines, such as suse or redhat, you may 
need "./configure ../.. --enable-libsuffix=64" instead.)
This will fetch the architecture type for which GAP has been  compiled
last and create a `Makefile'. Now call

  make

to compile the binary and to install it in the appropriate place.

This completes the installation of GUAVA for a single architecture. If
you use this installation of GUAVA on different hardware platforms you
will have to compile the binaries for each platform  separately.  This
is done by  calling  `configure'  and  `make'  for  the  package  anew
immediately  after   compiling   GAP   itself   for   the   respective
architecture. If your version of GAP is already compiled (and has last
been compiled on the same architecture) you do not need to compile GAP
again; it is sufficient to call the `configure' script in the GAP home
directory.

                             Loading GUAVA
                             -------------

After starting up GAP, the GUAVA package  needs  to  be  loaded.  Load
GUAVA by typing at the GAP prompt:

  gap> LoadPackage( "guava" );

If GUAVA isn't already in memory,  it  is  loaded  and  its  beautiful
banner is displayed.

You may wish to test the installation by reading in the Guava test file:

  gap> Read("full-path/guava.tst");

If you  are a frequent user of  GUAVA, you might consider putting this
line in your `.gaprc' file.

                              Bug reports
                              -----------

When sending a bug report to support@gap-system.org, remember we
will need to be able to reproduce the problem; so please include:

 * The version of GAP you are using; either look at  the  header  when
   you start up GAP, or at the gap> prompt type: VERSION;
 * The operating system you are using e.g. Linux, SunOS 5.8 =  Solaris
   2.8, IRIX 6.5, Windows, ...
 * The compiler (if any) you used to compile the binaries and the options  
   you used. Type: gcc -v or: cc -version.
 * A script that demonstrates the bug, along with a description of why
   it's a bug  (e.g.  by  adding  comments  to  the  script  -  recall
   comments in GAP  begin with a #).

  - David Joyner (wdjoyner@gmail.com)
    Junee 22, 2008

